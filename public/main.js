$(function() {
  // Initialize variables / socket.io
  var $messages = $(".messages"); // Messages area
  var socket = io();
  var FADE_TIME = 150; // ms

  log("[welcome] Please open the App and use the submit button to proceed...");

  // Logs a message
  function log(message, options) {
    var $el = $("<div>")
      .addClass("alert alert-info")
      .text(message);
    addMessageElement($el, options);
  }

  // Adds a message element to the messages and scrolls to the bottom
  // el - The element to add as a message
  // options - If location data is present include Google Places Map with Marker
  function addMessageElement(el, options) {
    var $el = $(el);
    $messages.append($el);
    if (options) {
      $(
        '<iframe width="500" height="150" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=' +
          options.latitude +
          "," +
          options.longitude +
          '&key=AIzaSyAtqbZ6pGc_HHQY9KgGSFNi5yTDpO0ywro" allowfullscreen </iframe>'
      ).appendTo(".messages");
    }
    $messages[0].scrollTop = $messages[0].scrollHeight;
  }

  // Socket events
  // Whenever the server emits 'new message', update the view
  socket.on("new message", function(data) {
    log(
      "[update] Time: " +
        new Date().toUTCString() +
        ", Coordinates: (lat, long) = (" +
        data.latitude +
        ", " +
        data.longitude +
        ")",
      data
    );
  });

  socket.on("disconnect", function() {
    log("[server] You have been disconnected...");
  });

  socket.on("reconnect", function() {
    log("[server] You have been reconnected...");
  });

  socket.on("reconnect_error", function() {
    log("[server] Attempt to reconnect has failed...");
  });
});
