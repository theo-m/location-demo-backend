var express = require("express");
var app = express();
var path = require("path");
var bodyParser = require("body-parser");
var port = process.env.PORT || 3000;
var io = require("socket.io").listen(app.listen(port));

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));

io.sockets.on("connection", function(socket) {
  socket.on("echo", function(data) {
    io.sockets.emit("message", data);
  });
});

require("./routes/api")(app, io);

console.log("Server listening at port " + port);
