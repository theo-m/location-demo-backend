# README #

Location Services backend for demo app.

The latest version of this app is hosted on heroku and available at https://harmony-api.herokuapp.com

### How do I get set up? ###

* On your local machine clone this repo and run npm install in the directory. 

* Fire the server up with nodemon or with node server.js.

### Who do I talk to? ###

* Theo