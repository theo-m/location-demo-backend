module.exports = function(app, io) {
  app.post("/location", function(req, res) {
    console.log(
      "[update] Time: " +
        new Date().toUTCString() +
        ", Coordinates: (lat, long) = (" +
        req.body.latitude +
        ", " +
        req.body.longitude +
        ")"
    );

    io.sockets.emit("new message", {
      latitude: parseFloat(req.body.latitude),
      longitude: parseFloat(req.body.longitude)
    });

    res.json({ result: "update sent over IO" });
  });
};
